FROM elasticsearch:1.5.2
MAINTAINER Matt Withum <matt@imaginecareers.com>
WORKDIR /usr/share/elasticsearch
RUN bin/plugin --install mobz/elasticsearch-head
COPY elasticsearch.yml /usr/share/elasticsearch/config/elasticsearch.yml
COPY ./entrypoint.sh /
RUN ["chmod", "+x", "/entrypoint.sh"]
ENTRYPOINT ["/entrypoint.sh"]

ENV CLUSTER_NAME es-prod-but-not-really
