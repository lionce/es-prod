#!/bin/bash

set -e

AWS_PRIVATE_IP=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
export PUBLISH_HOST=$AWS_PRIVATE_IP 

elasticsearch

# As argument is not related to elasticsearch,
# then assume that user wants to run his own process,
# for example a `bash` shell to explore this image
exec "$@"
